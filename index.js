var jwt = require('jsonwebtoken');
var tokenSecret = "token@rede@conc1l1ad0r"; 
 
exports.handler = function(event, context) { 
	
	var token = event.authorizationToken;
	
	jwt.verify(token, tokenSecret, function(err, decoded) {
		if(err){
			context.fail("Unauthorized");
		}else{
			context.succeed(generatePolicy(decoded.user, 'Allow', event.methodArn));
		}
		
	});
	
	
};

var generatePolicy = function(principalId, effect, resource) {
    var authResponse = {};
    authResponse.principalId = principalId;
    if (effect && resource) {
        var policyDocument = {};
        policyDocument.Version = '2012-10-17'; // default version
        policyDocument.Statement = [];
        var statementOne = {};
        statementOne.Action = 'execute-api:Invoke'; // default action
        statementOne.Effect = effect;
        statementOne.Resource = "*";
        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument;
    }
    return authResponse;
}